An example of how to use SDCC with CMake, using the framework
[stm8-bare-min](https://github.com/lujji/stm8-bare-min).

Requirements
------------
* You will need a more recent version of SDCC than that which is provided by
Debian Stretch.

* `stm8flash`

Setup
-----
Run `./bootstrap.sh` to run cmake with the required commands to setup the
`build` directory.
